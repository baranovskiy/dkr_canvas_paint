var Paint = function () {

    //setting properties, value = id
    var canvasId = 'canvas';
    var colorBox = 'color';
    var lineBox = 'line_width';
    var exportBox = 'export';
    var importBox = 'import';
    var clearAllBox = 'clear_all';
    //value = button ID
    var buttons     = {
        circle:     'circle',
        line:       'line',
        ellipse:    'ellipse',
        rect:       'rect',
        pencil:     'pencil'
    };
    var currentColor = "#000000";

    var currentLineWidth = 1;

    var canvasStorage = {};

    var ctx                 = null;
    var colorBoxElement     = null;
    var lineWidthElement    = null;
    var canvasElement       = null;
    var currentFigureType   = null;
    var exportButton        = null;
    var importButton        = null;
    var clearAllButton      = null;
    var canPaint            = false;
    var currentFigureId     = 1;

    var currentXStart = 0;
    var currentYStart = 0;

    var currentX = 0;
    var currentY = 0;

    var canvasWidth = 0;
    var canvasHeight = 0;

    function initialize()
    {
        //getting buttons for bind
        canvasElement = document.getElementById(canvasId);
        colorBoxElement = document.getElementById(colorBox);
        lineWidthElement = document.getElementById(lineBox);
        exportButton = document.getElementById(exportBox);
        importButton = document.getElementById(importBox);
        clearAllButton = document.getElementById(clearAllBox);

        ctx = canvasElement.getContext("2d");
        ctx.strokeStyle = currentColor;
        ctx.lineWidth = currentLineWidth;
        ctx.lineJoin = "round";

        canvasHeight = ctx.canvas.height;
        canvasWidth = ctx.canvas.width;

        //binding buttons
        for (var i in buttons)
        {
            var buttonElement = document.getElementById(buttons[i]);
            buttonElement.setAttribute("data-figure-type", i);
            buttonElement.addEventListener('click', changeFigureType , false);
        }
        //adding event listeners
        colorBoxElement.addEventListener('change', changeColor);
        lineWidthElement.addEventListener('change', changeLineWidth);
        canvasElement.addEventListener("mousedown", canvasMouseDown);
        canvasElement.addEventListener("mouseleave", canvasMouseLeave);
        canvasElement.addEventListener("mouseup", canvasMouseUp);
        canvasElement.addEventListener("mousemove", canvasMouseMove);
        clearAllButton.addEventListener("click", clearAll);
        exportButton.addEventListener("click", exportJSON);
        importButton.addEventListener("change", importJSON);
    }

    function clearAll()
    {
        canvasStorage = {};
        currentFigureId = 1;
        clearCanvas();
    }

    function clearCanvas()
    {
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);
    }
    //changing figure type and adding classes
    function changeFigureType()
    {
        currentFigureType = this.getAttribute('data-figure-type');
        var buttons = document.querySelectorAll("#controls button");
        [].forEach.call(buttons, function(element) {
            removeClass(element, 'selected-figure');
        });
        this.setAttribute('class', 'selected-figure');
    }

    function removeClass(ele, cls) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg,' ');
    }

    function changeColor()
    {
        currentColor = this.value;
        ctx.strokeStyle = currentColor;
    }
    function changeLineWidth()
    {
        currentLineWidth = this.value;
        ctx.lineWidth= currentLineWidth;
    }

    //need to adding data in canvasStorage
    function canvasMouseMove(e)
    {
        if (canPaint)
        {
            currentX = e.pageX - this.offsetLeft;
            currentY = e.pageY - this.offsetTop;
            switch(currentFigureType)
            {
                case 'circle':
                case 'line':
                case 'ellipse':
                case 'rect':
                    addFigureDataToCanvasStorage({
                        x1:currentXStart,
                        y1:currentYStart,
                        x2:currentX,
                        y2:currentY
                    });
                    break;
                case 'pencil':
                    addFigureDataToCanvasStorage({
                        dotPositionX:currentX,
                        dotPositionY:currentY
                    });
                    break;
            }
            drawAll(canvasStorage);
        }
    }


    function addFigureDataToCanvasStorage(figureData) {

        if (!canvasStorage.hasOwnProperty(String(currentFigureId)))
        {
            canvasStorage[currentFigureId] = {
                type: currentFigureType,
                color: currentColor,
                lineWidth: currentLineWidth,
                figureData: []
            };
        }
        canvasStorage[currentFigureId].figureData.push(figureData);
    }

    function canvasMouseDown(e)
    {
        currentFigureId+=1;
        if (currentFigureType != null)
        {
            currentXStart = e.pageX - this.offsetLeft;
            currentYStart= e.pageY - this.offsetTop;
            canPaint = true;
        }
        else
        {
            alert("Select a figure");
        }
    }

    function canvasMouseLeave()
    {
        canPaint = false;
    }

    function canvasMouseUp()
    {
        canPaint = false;
    }

    function drawCircle(circle)
    {
        var circleData = circle[circle.length-1];
        ctx.beginPath();
        ctx.arc(circleData.x1, circleData.y1, Math.abs((circleData.x2 - circleData.x1) / 2), 0, Math.PI*2, false);
        ctx.closePath();
        ctx.stroke();
    }

    function drawLine(line)
    {
        var lineData = line[line.length - 1];
        ctx.beginPath();
        ctx.moveTo(lineData.x1, lineData.y1);
        ctx.lineTo(lineData.x2, lineData.y2);
        ctx.closePath();
        ctx.stroke();
    }

    function drawEllipse(ellipse)
    {
        var ellipseData = ellipse[ellipse.length-1];
        var radiusX = (ellipseData.x2 - ellipseData.x1) * 0.5,
            radiusY = (ellipseData.y2 - ellipseData.y1) * 0.5,
            centerX = ellipseData.x1 + radiusX,
            centerY = ellipseData.y1 + radiusY,
            step = 0.01,
            a = step,
            pi2 = Math.PI * 2 - step;

        ctx.beginPath();

        ctx.moveTo(centerX + radiusX * Math.cos(0),
            centerY + radiusY * Math.sin(0));

        for(; a < pi2; a += step) {
            ctx.lineTo(centerX + radiusX * Math.cos(a),
                centerY + radiusY * Math.sin(a));
        }

        ctx.closePath();
        ctx.stroke();
    }

    function drawRect(rectangle)
    {
        var rectangleData = rectangle[rectangle.length-1];
        ctx.beginPath();
        ctx.strokeRect(
            rectangleData.x1,
            rectangleData.y1,
            rectangleData.x2-rectangleData.x1,
            rectangleData.y2-rectangleData.y1);
        ctx.closePath();
        ctx.stroke();
    }

    //sub function of pencil
    function drawPencil(dots)
    {
        if (dots.length)
        {
            for (var i = 0; i < dots.length; i++)
            {
                if (dots[i - 1] != null)
                {
                    ctx.beginPath();
                    if (dots[i] && i)
                    {
                        ctx.moveTo(dots[i - 1].dotPositionX, dots[i - 1].dotPositionY);
                    }
                    else
                    {
                        ctx.moveTo(dots[i].dotPositionX - 1, dots[i].dotPositionY);
                    }
                    ctx.lineTo(dots[i].dotPositionX, dots[i].dotPositionY);
                    ctx.closePath();
                    ctx.stroke();
                }
            }
        }
    }
    //calling every event
    function drawAll()
    {
        clearCanvas();
        for (var i in canvasStorage) {
            if (canvasStorage[i] != null) {
                ctx.strokeStyle = canvasStorage[i].color;
                ctx.lineWidth = canvasStorage[i].lineWidth;
                var currentFigureData = canvasStorage[i].figureData;
                switch (canvasStorage[i].type)
                {
                    case 'pencil':
                        drawPencil(currentFigureData);
                        break;
                    case 'rect':
                        drawRect(currentFigureData);
                        break;
                    case 'ellipse':
                        drawEllipse(currentFigureData);
                        break;
                    case 'circle':
                        drawCircle(currentFigureData);
                        break;
                    case 'line':
                        drawLine(currentFigureData);
                        break;
                }
            }
        }

    }

    //export json as usual text file, format: .json
    function exportJSON()
    {
        var jsonData = JSON.stringify(canvasStorage);
        var link = document.createElement('a');
        var mimeType = 'text/plain';

        link.setAttribute('download', "export.json");
        link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(jsonData));
        link.click();
        console.log(link);
    }

    //importing json from file
    function importJSON()
    {
        var jsonFile = this.files[0];
        var fileReader = new FileReader();
        fileReader.onload = afterImportFileLoad;
        fileReader.readAsText(jsonFile);
    }

    function afterImportFileLoad()
    {
        clearAll();
        canvasStorage = JSON.parse(this.result);
        //getting last figure id of new json !important!
        for (i in canvasStorage);
        currentFigureId = i;
        drawAll();
    }

    //starting here
    initialize();
};

//on document load - load paint class
window.addEventListener('load', function () {
    Paint();
}, false);

//for firefox
HTMLElement.prototype.click = function() {
    var evt = this.ownerDocument.createEvent('MouseEvents');
    evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    this.dispatchEvent(evt);
}